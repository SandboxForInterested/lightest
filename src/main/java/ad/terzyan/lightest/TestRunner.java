package ad.terzyan.lightest;

import ad.terzyan.lightest.annotation.TestClass;
import ad.terzyan.lightest.processor.TestCombiner;
import org.reflections.Reflections;

import java.util.Set;

public class TestRunner {

    public static void start(String pkg){
        Reflections reflections = new Reflections(pkg);
        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(TestClass.class);
        new TestCombiner(annotated).produce().printResult();
    }
}
