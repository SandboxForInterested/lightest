package ad.terzyan.lightest.processor;

import java.util.ArrayList;
import java.util.Collection;

public class TestCombiner {
    private final Collection<Class<?>> testClasses;
    private Collection<TestResult> results;

    public TestCombiner(Collection<Class<?>> testClasses){
        this.testClasses = testClasses;
        this.results = new ArrayList<>();
    }

    public TestCombiner produce(){
        for(Class testClass: testClasses)
            results.add(new TestProcessor(testClass).operate().getResult());
        return this;
    }

    public String getResult(){
        StringBuilder result = new StringBuilder();
        for(TestResult tr: results){
            result.append(tr.getResultOfTest());
        }
        return result.toString();
    }

    public Collection<TestResult> getResults() {
        return results;
    }

    public void printResult(){
        System.out.println(getResult());
    }
}
