package ad.terzyan.lightest.processor;

import ad.terzyan.lightest.annotation.Test;

import java.lang.reflect.Method;

public class TestProcessor {

    private Class testClass;
    private TestResult result;
    private long startTestingTime;

    public TestProcessor(Class testClass){
        this.testClass = testClass;
        result = new TestResult(testClass);
    }

    public TestProcessor operate(){
        setTestingTime();
        for (Method method : testClass.getDeclaredMethods()) {
            testMethod(method);
        }
        result.setTestingNanoTime(cutTestingTime());
        return this;
    }

    public TestResult getResult() {
        return result;
    }

    private void setTestingTime(){
        this.startTestingTime = System.nanoTime();
    }
    private long cutTestingTime(){
        return System.nanoTime() - startTestingTime;
    }

    private void testMethod(Method method){
        if (method.isAnnotationPresent(Test.class)) {
            try {
                method.invoke(testClass.newInstance());
                result.setMethodTestResult(method.getName());
            } catch (Exception e) {
                result.setMethodTestResult(method.getName(),e.getMessage());
            }
        }
    }
}
