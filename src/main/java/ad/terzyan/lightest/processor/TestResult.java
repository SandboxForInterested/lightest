package ad.terzyan.lightest.processor;

import java.util.HashMap;
import java.util.Map;

public class TestResult {

    private Class testedClass;
    private Map<String,String> methodTestResult;
    private int passedTests;
    private long testingTime;

    public TestResult(Class testedClass){
        this.testedClass = testedClass;
        methodTestResult = new HashMap<>();
    }

    public void setMethodTestResult(String methodName, String... exceptionMsg){
        if(exceptionMsg == null || exceptionMsg.length == 0){
            methodTestResult.put(methodName,"pass");
            passedTests++;
        }else{
            methodTestResult.put(methodName,"failed with reason "+exceptionMsg[0]);
        }
    }

    public void setTestingNanoTime(long testingTime){
        this.testingTime = testingTime;
    }

    public int getPassedTests(){
        return passedTests;
    }

    public String getResultOfTest(){
        StringBuilder result = new StringBuilder(String.format("Test class '%s' start\n", testedClass.getSimpleName()));
        for(String key:methodTestResult.keySet()){
            result.append(String.format("Test %s method. Result: %s",key,methodTestResult.get(key))).append("\n");
        }
        result.append(
                String.format(
                        "Result of test %s class: pass %d of %d running time %d ms"
                        , testedClass.getSimpleName()
                        , passedTests
                        , methodTestResult.size()
                        , testingTime / (1_000_000)
                ));
        return result.toString();
    }
}
