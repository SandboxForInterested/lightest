package ad.terzyan.lightest;

import ad.terzyan.lightest.processor.TestResult;
import ad.terzyan.lightest.testexample.testcases.SimpleClassTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestClassForTestResult {

    private final String result = "Test class 'SimpleClassTest' start\n" +
            "Test testMethodAlwaysOne method. Result: pass\n" +
            "Result of test SimpleClassTest class: pass 1 of 1 running time 1 ms";


    @Test
    public void checkPassedTest(){
        TestResult testResult = new TestResult(SimpleClassTest.class);
        testResult.setMethodTestResult("testMethodAlwaysOne");
        Assertions.assertEquals(1,testResult.getPassedTests());
    }

    @Test
    public void checkFailedTest(){
        TestResult testResult = new TestResult(SimpleClassTest.class);
        testResult.setMethodTestResult("testMethodAlwaysOne","NPE");
        Assertions.assertEquals(0,testResult.getPassedTests());
    }


    @Test
    public void checkResultMessage(){
        TestResult testResult = new TestResult(SimpleClassTest.class);
        testResult.setMethodTestResult("testMethodAlwaysOne");
        testResult.setTestingNanoTime(1_000_000);
        Assertions.assertEquals(result,testResult.getResultOfTest());
    }
}
