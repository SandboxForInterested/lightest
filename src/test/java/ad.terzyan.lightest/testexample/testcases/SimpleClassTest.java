package ad.terzyan.lightest.testexample.testcases;

import ad.terzyan.lightest.annotation.Test;
import ad.terzyan.lightest.annotation.TestClass;
import ad.terzyan.lightest.testexample.pojo.SimpleClass;

@TestClass
public class SimpleClassTest {

    @Test
    public void testMethodAlwaysOne(){
        assert new SimpleClass().alwaysOne() == 1;
    }
}