package ad.terzyan.lightest.testexample;

import ad.terzyan.lightest.TestRunner;

public class Main {

    public static void main(String... args){
        TestRunner.start("ad.terzyan.testexample");
    }
}
