package ad.terzyan.lightest;

import ad.terzyan.lightest.processor.TestCombiner;
import ad.terzyan.lightest.processor.TestProcessor;
import ad.terzyan.lightest.testexample.testcases.SimpleClassTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Collections;

public class TestClassForTestCombiner {

    @Test
    public void checkResultStringProduceTestCombiner(){
        TestCombiner testCombiner = new TestCombiner(Collections.singleton(SimpleClassTest.class));
        testCombiner.produce();
        Assertions.assertEquals(1,testCombiner.getResults().size());
    }
}
