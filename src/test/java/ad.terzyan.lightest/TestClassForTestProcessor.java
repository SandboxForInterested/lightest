package ad.terzyan.lightest;

import ad.terzyan.lightest.processor.TestProcessor;
import ad.terzyan.lightest.testexample.testcases.SimpleClassTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestClassForTestProcessor {

    @Test
    public void checkResultStringProduceTestProcessor(){
        TestProcessor testProcessor = new TestProcessor(SimpleClassTest.class);
        testProcessor.operate();
        Assertions.assertEquals(1,testProcessor.getResult().getPassedTests());
    }
}
